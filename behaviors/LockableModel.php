<?php namespace StudioBosco\LockRecords\Behaviors;

use BackendAuth;
use Winter\Storm\Extension\ExtensionBase;
use StudioBosco\LockRecords\Models\LockedRecord;
use StudioBosco\LockRecords\Models\PendingChange;

class LockableModel extends ExtensionBase
{
    use \StudioBosco\LockRecords\Traits\Lockable;

    protected $model;

    public function __construct($parent)
    {
        $this->model = $parent;
        $this->bootLockable();
    }

    public function lock($user = null)
    {
        $user = $user ?: BackendAuth::getUser();

        if (!$user) {
            return;
        }

        if ($this->isLocked($user)) {
            return;
        }

        $lock = $this->model->recordLock ?: new LockedRecord();
        $lock->record = $this->model;
        $lock->editor = $user;
        $lock->touch();
        $lock->save();
    }

    public function getLock()
    {
        return $this->model->recordLock;
    }

    public function queueChanges()
    {
        $changes = $this->model->getDirty();

        $this->model->pendingChanges()->create([
            'changed_attributes' => $changes,
        ]);
    }

    public function applyPendingChanges()
    {
        if (!$this->model->pendingChanges()->count()) {
            return;
        }

        Log::info('Applying pending changes of previously locked ' . $this->model::class . ' with ID ' . $this->model->id . '.');

        foreach($this->model->pendingChanges()->get() as $record) {
            $changes = $record->changed_attributes;

            foreach($changes as $key => $value) {
                $this->model->$key = $value;
            }
        }

        $this->model->pendingChanges()->delete();
        $this->model->save();
    }

    /**
     * Boot the lockable behavior for a model.
     * @return void
     */
    public function bootLockable()
    {
        $self = $this;

        // attach relationship
        $this->model::extend(function ($model) {
            $model->morphOne['recordLock'] = [
                LockedRecord::class,
                'name' => 'record',
            ];
            $model->morphMany['pendingChanges'] = [
                PendingChange::class,
                'name' => 'record',
                'order' => 'created_at asc',
            ];
            $model->addDynamicMethod('scopeIsLocked', function ($query, $user = null) {
                $user = $user ?: BackendAuth::getUser();

                if ($user) {
                    return $query->whereHas('lock', function ($query) use ($user) {
                        $query->whereNot('editor_id', $user->id);
                    });
                } else {
                    return $query->whereHas('lock');
                }
            });
            $model->addDynamicMethod('scopeIsUnlocked', function ($query, $user = null) {
                $user = $user ?? BackendAuth::getUser();

                if ($user) {
                    return $query->whereHasNot('lock', function ($query) use ($user) {
                        $query->whereNot('editor_id', $user->id);
                    });
                } else {
                    return $query->whereHasNot('lock');
                }
            });
        });

        // prevent model from updating if it is locked
        $this->model::updating(function ($model) use ($self) {
            if ($model->isLocked()) {

                Log::warning('Record of type ' . $model::class . ' with ID ' . $model->id . ' is locked by user ' . $model->getLock()->editor->login . '. Changes will be applied once unlocked.');
                $self->queueChanges();

                $user = BackendAuth::getUser();

                if ($user) {
                    Flash::warning(trans('studiobosco.lockrecords::lang.messages.record_locked_when_updating', ['record_name' => $model::class, 'record_id' => $model->id, 'editor_name' => $model->getLock()->editor->login]));
                }
                return false;
            }
        });

        // delete pending changes when the model get's deleted
        $this->model::deleting(function ($model) {
            $model->pendingChanges()->delete();
        });
    }
}
