<?php namespace StudioBosco\LockRecords\Behaviors;

use Flash;
use Backend;
use BackendAuth;
use Backend\Classes\ControllerBehavior;

class LockRecordsController extends ControllerBehavior
{
    /**
     * @var bool Has the behavior been initialized.
     */
    protected $initialized = false;

    /**
     * @var \Backend\Classes\Controller Reference to the back end controller.
     */
    protected $controller;

    public function __construct($controller)
    {
        parent::__construct($controller);

        $controller->addCss('$/studiobosco/lockrecords/assets/css/lockrecords.css');

        $controller::extendFormFields(function ($form, $model, $context) {
            if ($form->isNested || !$model->methodExists('isLocked')) {
                return;
            }

            if ($model->isLocked()) {

                // define position where to place the fields
                $position = 0;

                // get data
                $before = $form->fields;
                $after = array_splice($before, $position);

                // add lock field
                $newFields = [
                    '_locked_by' => [
                        'type' => 'partial',
                        'path' => '$/studiobosco/lockrecords/partials/_field_locked_by',
                        'span' => 'full',
                    ]
                ];

                // add unlock field if user has Access
                $user = BackendAuth::getUser();
                if ($user && $user->hasAccess('studiobosco.lockrecords::manually_unlock_records')) {
                    $newFields['_unlock'] = [
                        'type' => 'partial',
                        'path' => '$/studiobosco/lockrecords/partials/_field_unlock',
                        'span' => 'full',
                    ];
                }

                // remove all fields
                foreach ($form->fields as $fieldName => $definition) {
                    $form->removeField($fieldName);
                }

                // repopulate fields
                $merged = array_merge($before, $newFields, $after);
                foreach ($merged as $fieldName => $definition) {
                    $form->addFields([
                        $fieldName => $definition,
                    ]);
                }

            }

        });
    }

    public function onLockRecord($recordId)
    {
        $record = $this->controller->formFindModelObject($recordId);

        if ($record) {
            $record->lock();
        }
    }

    public function onUnlockRecord($recordId)
    {
        $record = $this->controller->formFindModelObject($recordId);

        if ($record) {
            $record->unlock();

            if (post('redirect_to')) {
                switch(post('redirect_to')) {
                    case 'update': {
                        $redirectPath = str_replace('/controllers/', '/', str_replace('\\', '/', strtolower($this->controller::class))) . '/update/' . $recordId;
                        Flash::success(trans('studiobosco.lockrecords::lang.messages.unlock_record_success'));

                        return redirect(Backend::url($redirectPath));
                    }
                }
            }
        }
    }

    public function lockRecordsUpdate($recordId = null, $context = null)
    {
        $this->addJs('js/lockrecords.js');

        if ($recordId) {
            $record = $this->controller->formFindModelObject($recordId);

            if ($record && $record->isLocked()) {
                $redirectPath = str_replace('/controllers/', '/', str_replace('\\', '/', strtolower($this->controller::class))) . '/preview/' . $recordId;

                return redirect(Backend::url($redirectPath));
            }
        }

        return $this->controller->asExtension('FormController')->update($recordId, $context);
    }

    public function update($recordId = null, $context = null)
    {
        return $this->lockRecordsUpdate($recordId, $context);
    }

    public function lockRecordsInjectRowClass($record)
    {
        if ($record->methodExists('isLocked') && $record->isLocked()) {
            return 'locked';
        }
    }

    public function listInjectRowClass($record)
    {
        return $this->lockRecordsInjectRowClass($record);
    }
}
