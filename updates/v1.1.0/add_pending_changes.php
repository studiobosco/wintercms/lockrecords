<?php

use Winter\Storm\Database\Schema\Blueprint;
use Winter\Storm\Database\Updates\Migration;
use Winter\Storm\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studiobosco_lockrecords_pending_changes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->bigInteger('record_id')->unsigned();
            $table->string('record_type', 191);
            $table->json('changed_attributes')->nullable();
            $table->timestamps();
            $table->index('record_id');
            $table->index('record_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studiobosco_lockrecords_pending_changes');
    }
};
