<?php namespace StudioBosco\LockRecords\Updates;

use Schema;
use Winter\Storm\Database\Schema\Blueprint;
use Winter\Storm\Database\Updates\Migration;

class V101 extends Migration
{
    public function up()
    {
        Schema::table('studiobosco_locked_records', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('record_type', 191)->change();
            $table->index('editor_id');
            $table->index('record_id');
            $table->index('record_type');
        });
    }

    public function down()
    {
        Schema::table('studiobosco_locked_records', function (Blueprint $table) {
            $table->dropIndex([
                'editor_id',
                'record_id',
                'record_type',
            ]);
            $table->string('record_type', 1024)->change();
        });
    }
}
